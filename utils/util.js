const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return [year, month, day].map(formatNumber).join('-')
}

const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : '0' + n
}

// 五星好评
function starsArray(stars) {
  var array = [];
  var decimal = parseInt(stars.toString().substring(2)); //取小数点后一位
  // console.log(decimal)
  var integer = Math.floor(stars);
  for (var i = 1; i <= integer; i++) {
    array.push(2)
  }
  if (decimal > 5) {
    integer++
    array.push(2)
  } else if (decimal > 0 && decimal <= 5) {
    array[integer] = 1
  }
  while (array.length < 5) {
    array.push(0)
  }
  return array;
}

// 信誉评分
function creditNum(num) {
  if (num <= 2) {
    return '信誉一般'
  } else if (num > 2 && num <= 3) {
    return '信誉好'
  } else if (num > 3 && num <= 4) {
    return '信誉良好'
  } else {
    return '信誉极好'
  }
}

// 当前时间
function getNowTime() {
  var now = new Date();
  var year = now.getFullYear();
  var month = now.getMonth() + 1;
  var day = now.getDate();
  if (month < 10) {
    month = '0' + month;
  };
  if (day < 10) {
    day = '0' + day;
  };
  //  如果需要时分秒，就放开
  // var h = now.getHours();
  // var m = now.getMinutes();
  // var s = now.getSeconds();
  var formatDate = year + '-' + month + '-' + day;
  return formatDate;
}

/**
 * 重复点击事件处理
 */
function repeatBtn(fn, gapTime) {
  if (gapTime == null || gapTime == undefined) {
    gapTime = 1500
  }

  let _lastTime = null

  // 返回新的函数
  return function() {
    let _nowTime = +new Date()
    if (_nowTime - _lastTime > gapTime || !_lastTime) {
      fn.apply(this, arguments) //将this和参数传给原函数
      _lastTime = _nowTime
    }
  }
}

// 日期相减
function DateMinus(sDate, eDate) {　　
  var sdate = new Date(sDate.replace(/-/g, "/"));　
  var edate = new Date(eDate.replace(/-/g, "/"));
  var days = edate.getTime() - sdate.getTime();　　
  var day = parseInt(days / (1000 * 60 * 60 * 24));　　
  return day;
}

// yy-mm-dd 转 mm月dd日
function turnDate(date) {
  let newDate = date.split('-');
  newDate = newDate[1] + '月' + newDate[2] + '日';
  return newDate;
}

module.exports = {
  formatTime: formatTime,
  starsArray: starsArray,
  getNowTime: getNowTime,
  creditNum: creditNum,
  repeatBtn: repeatBtn,
  DateMinus: DateMinus,
  turnDate: turnDate
}