// pages/index/index.js
const app = getApp();
let utils = require('../../utils/utils');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    imgUrls: [],
    inDate: '',
    outDate: '',
    totalNight: '',
    inTime: '',
    outTime: '',
    siteName: '',
    addrName: '',
    tags: [],
    tagId: '',
    currentIdx: null,
    homestayId: '',
    hotelList: [],
    startDate: '',
    endDate: '',
    locationId: '',
    freeTicket: [],
    //scope：酒店1 景点2
    scope: 1
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(e) {},

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {
    // 自动发放优惠券
    this.autoCoupons();
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    this.getHomestayMsg(), this.getTags();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },

  // 选择日期
  checkDate() {
    wx.navigateTo({
      url: '/pages/calendar-homestay/calendar-homestay'
    })
  },

  // 获取酒店信息、
  getHomestayMsg() {
    let that = this;
    utils.request({
      url: 'hotel/getHotel',
      method: 'GET',
      success: function(res) {
        let r = res.data;
        if (r.success) {
          let data = r.data;
          that.setData({
            imgUrls: data.url,
            siteName: data.name,
            addrName: data.address,
            homestayId: data.id,
            locationId: data.locationId
          })
          // 渲染酒店列表
          that.getHomestayLists();
        }
      }
    })
  },

  // 获取标签
  getTags() {
    let that = this;
    utils.request({
      url: 'hotel/getTags',
      method: 'GET',
      success: function(res) {
        let r = res.data;
        if (r.success) {
          let data = r.data;
          that.setData({
            tags: data
          })
        }
      }
    })
  },

  // 选择标签
  checkTag(e) {
    let tagId = e.currentTarget.dataset.id;
    let idx = e.currentTarget.dataset.index;
    let that = this;
    // console.log(tagId);
    that.setData({
      currentIdx: idx
    });
    // 标签筛选酒店列表
    that.getHomestayLists(tagId);
  },

  // 获取酒店列表
  getHomestayLists(tagId) {
    let that = this;
    let startDate = that.data.inTime;
    let endDate = that.data.outTime;
    let hotelId = that.data.homestayId;
    // console.log(that.data.homestayId);
    if (tagId == undefined) {
      tagId = '';
    }
    utils.request({
      url: 'hotel/getRoom',
      data: {
        endDate,
        startDate,
        hotelId,
        tagId
      },
      success: function(res) {
        let r = res.data;
        if (r.success) {
          let data = r.data;
          that.setData({
            hotelList: data.list,
            endDate: data.endDate,
            startDate: data.startDate
          })
        }
      }
    })
  },

  // 点击显示地图
  goMap(e) {
    // console.log(e);
    let locationId = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '../map/map?id=' + locationId
    })
  },

  onSubmitOrder(e) {
    let price = e.currentTarget.dataset.money;
    let id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '/pages/submit-order-homestay/submit-order-homestay?id=' + id + '&startDate=' + this.data.startDate + '&endDate=' + this.data.endDate + '&price=' + price
    })
  },

  // 查看详情
  payNotice(e) {
    let notesurl = e.currentTarget.dataset.notesurl;
    wx.navigateTo({
      url: '../payNotice/payNotice?notesurl=' + notesurl
    })
  },

  // 优惠券
  goCouponsDetail() {
    let that = this;
    wx.navigateTo({
      url: '../get-coupons/get-coupons?scope=' + that.data.scope
    })
  },

  // 赠景区门票 获取赠票信息
  bindFreeTicket(e) {
    let roomId = e.currentTarget.dataset.id;
    let that = this;
    utils.request({
      url: 'hotel/giveTicketInfo',
      data: {
        roomId
      },
      success: function(res) {
        let r = res.data;
        if (r.success) {
          let data = r.data;
          that.setData({
            freeTicket: data
          })
        }
      }
    })
    that.setData({
      showMask: true,
      showFreeModel: true
    })
  },

  // 自动发放优惠券
  autoCoupons() {
    utils.request({
      url: 'promotions/coupon/auto',
      success: function(res) {
        let r = res.data;
        if (r.success) {
          // console.log(r.msg);
        }
      }
    })
  },
  
})