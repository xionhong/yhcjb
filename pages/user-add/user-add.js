// pages/user-add/user-add.js
let utils = require('../../utils/utils');
let util = require('../../utils/util');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    cardTypeList: [],
    // idcardType: '',
    // idcardName: '',
    // idcard: '',
    realName: '',
    phone: '',
    userId: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(e) {
    let userId = e.userId;
    if (userId) {
      this.setData({
        userId: e.userId
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    let userId = this.data.userId;
    if (userId) {
      this.showUserMsg();
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },

  // 显示游客信息
  showUserMsg() {
    let that = this;
    let id = that.data.userId;
    utils.request({
      url: 'member/passengerById',
      data: {
        id
      },
      success: res => {
        let r = res.data;
        if (r.success) {
          let data = r.data;
          that.setData({
            // idcard: data.idcard,
            phone: data.phone,
            realName: data.realName,
            // idcardName: data.idcardTypeName,
            // idcardType: data.idcardType
          })
        }
      }
    })
  },

  //证件类型、
  // bindCardType() {
  //   let that = this;
  //   that.setData({
  //     showModel: true,
  //     showMask: true
  //   })
  //   utils.request({
  //     url: 'member/cardTypeList',
  //     success: function(res) {
  //       let r = res.data;
  //       if (r.success) {
  //         let data = r.data;
  //         that.setData({
  //           cardTypeList: data
  //         })
  //       }
  //     }
  //   })
  // },

  // 选择证件类型
  // gettype: function(e) {
  //   let that = this;
  //   let idcardType = e.detail.idcardType;
  //   let idcardName = e.detail.idcardName;
  //   that.setData({
  //     idcardType,
  //     idcardName
  //   })
  // },

  // 保存、修改
  formSubmit: util.repeatBtn(function(e) {
    // console.log(e);
    let that = this;
    // let idcard = e.detail.value.userId;
    let phone = e.detail.value.userPhone;
    // let idcardType = that.data.idcardType;
    let realName = e.detail.value.userName;
    // 用户id
    let id = that.data.userId;
    if (id == '') {
      id = -1
    }
    if (realName == undefined) {
      wx.showModal({
        title: '提示',
        content: '姓名不能为空',
        showCancel: false
      })
      return
    }
    // else if (idcardType == '') {
    //   wx.showModal({
    //     title: '提示',
    //     content: '还没选择证件类型',
    //     showCancel: false
    //   })
    //   return
    // } else if (idcard == undefined) {
    //   wx.showModal({
    //     title: '提示',
    //     content: '证件号码不能为空',
    //     showCancel: false
    //   })
    //   return
    // } 
    else if (phone == undefined) {
      wx.showModal({
        title: '提示',
        content: '手机号码不能为空',
        showCancel: false
      })
      return
    } else {
      utils.request({
        url: 'member/passengerAddOrUpdate',
        data: {
          // idcard,
          phone,
          realName,
          // idcardType,
          id
        },
        success: function(res) {
          let r = res.data;
          if (r.success) {
            wx.showModal({
              title: '提示',
              content: r.msg,
              showCancel: false,
              success: function() {
                wx.navigateBack({})
              }
            })
          } else {
            wx.showModal({
              title: '提示',
              content: r.msg,
              showCancel: false
            })
          }
        }
      })
    }
  }, 1000),

})