// pages/index/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    imgUrls: [
      '/images/ban1.png',
      '/images/ban2.png',
      '/images/ban3.png'
    ],
    inDate: '',
    outDate: '',
    totalNight: '',
    siteName: '景点名称',
    hotelList: [{
        id: '1',
        img: '/images/list1.png',
        name: '景点',
        num: '',
        money: '100',
        beforeMoney: ''
      },
      {
        id: '2',
        img: '/images/list1.png',
        name: '景点',
        num: '',
        money: '80',
        beforeMoney: ''
      }
    ]
  },

  checkDate() {
    wx.navigateTo({
      url: '/pages/calendar/calendar'
    })
  },

  // 拼团规则
  ruleDetails() {
    wx.navigateTo({
      url: '/pages/rule-details/rule-details'
    })
  },

  // 去参团
  goGroupBuying(e) {
    let orderid = '';
    wx.navigateTo({
      url: '/pages/submit-order/submit-order?orderid=' + orderid
    })
  },

  // 查看详情
  payNotice() {
    wx.navigateTo({
      url: '/pages/pay-notice/pay-notice'
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(e) {},

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    let that = this;
    wx.getStorage({
      key: 'checkDate',
      success: function(res) {
        let data = res.data;
        let month = data.month < 10 ? '0' + data.month + '月' : data.month + '月';
        let day = data.day < 10 ? '0' + data.day + '日' : data.day + '日';
        let nowMonth = data.nowMonth < 10 ? '0' + data.nowMonth + '月' : data.nowMonth + '月';
        let nowDay = data.nowDay < 10 ? '0' + data.nowDay + '日' : data.nowDay + '日';
        that.setData({
          date: month + day,
          nowDate: nowMonth + nowDay
        })
      }
    })

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})