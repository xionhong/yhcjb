let utils = require('../../utils/utils');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    imgUrls: [],
    siteName: '',
    addrName: '',
    spotId: '',
    ticketRestList: [],
    //scope：酒店1 景点2
    scope: 2
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(e) {
    this.setData({
      spotId: e.id,
    })
    wx.setNavigationBarTitle({
      title: e.name
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    this.getBanner();
    this.getSpotDetail();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },

  // 获取景点轮播图
  getBanner() {
    let that = this;
    let landmarkId = that.data.spotId;
    utils.request({
      url: 'landmark/getLandmarkSlide',
      data: {
        landmarkId
      },
      success: function(res) {
        let r = res.data;
        if (r.success) {
          let data = r.data;
          that.setData({
            imgUrls: data
          })
        }
      }
    })
  },

  // 领取优惠券
  goCouponsDetail() {
    let that = this;
    wx.navigateTo({
      url: '../get-coupons/get-coupons?scope=' + that.data.scope
    })
  },

  // 获取景点信息
  getSpotDetail() {
    let that = this;
    let landmarkId = that.data.spotId;
    utils.request({
      url: 'landmark/landmarkInfo',
      data: {
        landmarkId
      },
      success: function(res) {
        let r = res.data;
        if (r.success) {
          let data = r.data;
          that.setData({
            siteName: data.landmarkName,
            addrName: data.landmarkAddress,
            locationId: data.locationId,
            ticketRestList: data.ticketRestList
          })
        }
      }
    })
  },

  // 点击显示地图
  goMap(e) {
    // console.log(e);
    let locationId = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '../map/map?id=' + locationId
    })
  },

  // 查看详情
  payNotice(e) {
    // console.log(e);
    let notesurl = e.currentTarget.dataset.notesurl;
    wx.navigateTo({
      url: '../payNotice/payNotice?notesurl=' + notesurl
    })
  },
})