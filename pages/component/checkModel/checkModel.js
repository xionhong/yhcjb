// pages/component/checkModel/checkModel.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    showModel: {
      type: Boolean
    },
    showPlayer: {
      type: Boolean
    },
    showMask: {
      type: Boolean
    },
    showOrderDetail: {
      type: Boolean
    },
    showQrcode: {
      type: Boolean
    },
    showPower: {
      type: Boolean
    },
    showFreeModel: {
      type: Boolean
    },
    prices: {
      type: Array
    },
    num: {
      type: String
    },
    payPrice: {
      type: String
    },
    cardTypeList: {
      type: Array
    },
    idcardType: {
      type: String
    },
    userList: {
      type: Array
    },
    codeImg: {
      type: String
    },
    callUser: {
      type: String
    },
    freeTicket: {
      type: Array
    }
  },

  options: {
    addGlobalClass: true,
  },

  /**
   * 组件的初始数据
   */
  data: {
    num: null,
    prices: [],
    idcardType: '',
    userId: '',
    realName: '',
    idcard: '',
    phone: '',
    callUser: ''
  },

  /**
   * 组件的方法列表
   */
  methods: {
    closeModel() {
      let that = this;
      that.setData({
        showMask: false,
        showModel: false,
        showOrderDetail: false,
        showQrcode: false,
        showPlayer: false,
        showPower: false,
        showFreeModel: false
      })
    },

    // 选择
    radioChange(e) {
      let val = e.detail.value.split(',');
      let idcardType = val[0];
      let idcardName = val[1];
      if (val.length >= 3) {
        let realName = val[2];
        let phone = val[3];
        let idcard = val[4];
        this.setData({
          idcardType,
          idcardName,
          realName,
          phone,
          idcard
        })
      } else {
        this.setData({
          idcardType,
          idcardName
        })
      }

    },

    onConfirm() {
      let that = this;
      let idcardType = that.data.idcardType;
      let idcardName = that.data.idcardName;
      let realName = that.data.realName;
      let phone = that.data.phone;
      let idcard = that.data.idcard;
      // console.log(idcardType);
      that.setData({
        showMask: false,
        showModel: false,
        showPlayer: false
      })
      var myEventDetail = {
        idcardType: idcardType,
        idcardName: idcardName,
        realName: realName,
        phone: phone,
        idcard: idcard
      };
      if (realName || idcard || phone || idcardName || idcardType) {
        that.triggerEvent('ontype', myEventDetail) //这里的myevent要和调用该组件的事件命名一致 
      }
    },

    // 拨号
    callPhone() {
      let that = this;
      wx.makePhoneCall({
        phoneNumber: that.data.callUser
      })
    },

  }
})