// pages/component/addr.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    siteName: {
      type: String
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    addrName: '福建省厦门市思明区万石山风景'
  },

  /**
   * 组件的方法列表
   */
  methods: {

  }
})