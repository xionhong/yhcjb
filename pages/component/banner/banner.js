// pages/component.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    imgUrls: {
      type: Array
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    indicatorDots: false,
    autoplay: true,
    interval: 3000,
    duration: 1000,
    swiperCurrent: 0,
  },

  /**
   * 组件的方法列表
   */
  methods: {
    swiperChange(e) {
      let current = e.detail.current;
      // console.log(current, '轮播图')
      let that = this;
      that.setData({
        swiperCurrent: current,
      })
    },
  }
})