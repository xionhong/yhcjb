let utils = require('../../utils/utils');
let app = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    orderId: '',
    spotMsg: [],
    ticketList: [],
    codeImg: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(e) {
    let orderId = e.orderid;
    this.setData({
      orderId
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    this.getSpotMsg();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },

  // 获取景点详情
  getSpotMsg() {
    let that = this;
    let orderId = that.data.orderId;
    utils.request({
      url: 'order/tkOrderInfo',
      data: {
        orderId
      },
      success: function(res) {
        let r = res.data;
        if (r.success) {
          let data = r.data;
          that.setData({
            spotMsg: data.tkOrderInfoDTO,
            ticketList: data.orderTicketDTOList
          })
        }
      }
    })
  },

  // 显示二维码
  onShowQrcode(e) {
    console.log(e);
    let ticketCodeId = e.currentTarget.dataset.id;
    let codeImg = app.globalData.url + 'order/showQrCode?ticketCodeId=' + ticketCodeId;
    let that = this;
    that.setData({
      showMask: true,
      showQrcode: true,
      codeImg
    })

    // utils.request({
    //   url: 'order/showQrCode',
    //   data: {
    //     ticketCodeId
    //   },
    //   method: 'GET',
    //   success: function(res) {
    //     let data = res.data;
    //     console.log(data);
    //     that.setData({
    //       codeImg: ' + data
    //     })
    //   }
    // })
  }
})