// pages/index/index.js
const app = getApp();
let utils = require('../../utils/utils');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    imgUrls: [],
    navList: [],
    itemList: [],
    adList: [],
    showPower: false,
    getPhone: false,
    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    //公告跑马灯
    // aditem: ['盛开的交罚款了坚实的罗杰斯更合适的很干净开始的合格后说过话', '三等奖发货快圣诞节和福克斯的和疯狂和三等奖和发生的合肥将肯定是'],
    // str: '',
    // marqueePace: 1, //滚动速度
    // marqueeDistance: 0, //初始滚动距离
    // size: 24,
    // orientation: 'left', //滚动方向
    // interval: 20, // 时间间隔
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    // 授权
    this.getSetting();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    this.getBaanner(), this.getNav(), this.getHotelList(), this.getAd(), this.getUser();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {},

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {},

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {},

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {},

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {},

  getUser() {
    let that = this;

    wx.getSetting({
      success: (res) => {
        if (!res.authSetting['scope.userInfo']) {
          that.setData({
            showPower: true
          })
          wx.hideTabBar({})
        }
      }
    })
  },

  // 获取授权
  getSetting() {
    if (app.globalData.userInfo) {
      this.setData({
        userInfo: app.globalData.userInfo,
        hasUserInfo: true,
      })
    } else if (this.data.canIUse) {
      console.log()
      // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
      // 所以此处加入 callback 以防止这种情况
      app.userInfoReadyCallback = res => {
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        })
      }
    } else {
      // 在没有 open-type=getUserInfo 版本的兼容处理
      wx.getUserInfo({
        success: res => {
          app.globalData.userInfo = res.userInfo
          this.setData({
            userInfo: res.userInfo,
            hasUserInfo: true,
          })
        }
      })
    }
  },

  // 获取个人信息成功，然后处理剩下的业务或跳转首页
  getUserInfo: function(e) {
    let that = this;
    // console.log(e)
    app.globalData.userInfo = e.detail.userInfo
    that.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true
    })
    // console.log(e.detail.userInfo)
    if (e.detail.userInfo) {
      //用户按了允许授权按钮
      // console.log(e.detail.userInfo);
      let userInfo = e.detail.userInfo;
      utils.request({
        url: 'member/updateMember',
        data: {
          city: userInfo.city,
          country: userInfo.country,
          name: userInfo.nickName,
          photo: userInfo.avatarUrl,
          province: userInfo.province,
          sex: userInfo.gender
        },
        success: function(res) {}
      })
      that.setData({
        showPower: false,
        getPhone: true
      })
    } else {
      that.setData({
        showPower: false,
        getPhone: true
      })
    }
    wx.showTabBar({})
  },

  // 获取手机绑定的手机号码
  getPhoneNumber: function(e) {
    // console.log(e.detail.errMsg)
    // console.log(e.detail.iv)
    // console.log(e.detail.encryptedData)
    let that = this;
    utils.request({
      url: 'member/updateMemberPhone',
      data: {
        encryptedData: e.detail.encryptedData,
        ivStr: e.detail.iv
      },
      success: function(res) {
        that.setData({
          getPhone: false
        })
      },
      complate: function(res) {
        that.setData({
          getPhone: false
        })
      }
    })
  },

  // 获取轮播
  getBaanner() {
    let that = this;
    utils.request({
      url: 'index/queryApiSlideShow',
      method: 'GET',
      success: res => {
        let r = res.data;
        if (r.success) {
          let data = r.data;
          that.setData({
            imgUrls: data
          })
        }
      }
    })
  },

  // 获取公告
  getAd() {
    let that = this;
    utils.request({
      url: 'index/noticeShow',
      method: 'GET',
      success: res => {
        let r = res.data;
        if (r.success) {
          let data = r.data;
          that.setData({
            adList: data
          })
        }
      }
    })
    // let item = that.data.aditem;
    // let str = '';
    // item.forEach(item => {
    //   str += item + ' ';
    // })
    // let length = str.length * that.data.size; //文字长度
    // let windowWidth = wx.getSystemInfoSync().windowWidth; // 屏幕宽度
    // that.setData({
    //   length,
    //   windowWidth,
    //   str
    // });
    // that.runMarquee(); // 水平一行字滚动完了再按照原来的方向滚动
  },

  showAdDetail(e) {
    let idx = e.currentTarget.dataset.index;
    let that = this;
    wx.showModal({
      title: '公告',
      content: that.data.adList[idx],
      showCancel: false
    })
  },

  // 跑马灯
  // runMarquee() {
  //   var that = this;
  //   var interval = setInterval(function() {
  //     //文字一直移动到末端
  //     if (-that.data.marqueeDistance < that.data.length) {
  //       that.setData({
  //         marqueeDistance: that.data.marqueeDistance - that.data.marqueePace,
  //       });
  //     } else {
  //       clearInterval(interval);
  //       that.setData({
  //         marqueeDistance: that.data.windowWidth
  //       });
  //       that.runMarquee();
  //     }
  //   }, that.data.interval);
  // },

  // 获取导航
  getNav() {
    let that = this;
    utils.request({
      url: 'index/queryApiMenuList',
      method: 'GET',
      success: res => {
        let r = res.data;
        if (r.success) {
          let data = r.data;
          that.setData({
            navList: data
          })
        }
      }
    })
  },

  // 获取酒店列表
  getHotelList() {
    let that = this;
    utils.request({
      url: 'index/apiHotsGoods',
      method: 'GET',
      success: res => {
        let r = res.data;
        if (r.success) {
          let data = r.data;
          that.setData({
            itemList: data
          })
        }
      }
    })
  },

  // 跳转详情
  jumpDetail(e) {
    let hotsGoodId = e.currentTarget.dataset.id;
    let type = e.currentTarget.dataset.type;
    if (type == 1) {
      wx.navigateTo({
        url: "/pages/spotDetail/spotDetail?id=" + hotsGoodId
      })
    } else {
      wx.switchTab({
        url: '/pages/homestay/homestay'
      })
    }
  },

  // 限时抢购
  // time_limit(e) {
  //   wx.navigateTo({
  //     url: "/pages/spotDetail/spotDetail?id" + id
  //   })
  // },

  // 门票预订
  navGoDetail1() {
    wx.switchTab({
      url: '/pages/spot/spot'
    })
  },

  // 民宿预定
  navGoDetail2() {
    wx.switchTab({
      url: '/pages/homestay/homestay'
    })
  },

  // 公告须知
  navGoDetail3(e) {
    let notesurl = e.currentTarget.dataset.notesurl;
    wx.navigateTo({
      url: '../payNotice/payNotice?notesurl=' + notesurl
    })
  },

  // 优惠券列表
  navGoDetail4() {
    wx.navigateTo({
      url: '../get-coupons/get-coupons?scope=' + ''
    })
  },

  // 地图导航
  navGoDetail5() {
    wx.navigateTo({
      url: '../map/map'
    })
  },

  // 火爆拼团
  // navGoDetail8(e) {
  //   let id = e.currentTarget.dataset.id;
  //   wx.navigateTo({
  //     url: '/pages/activity-list/activity-list?title=火爆拼团' + '&id=' + id
  //   })
  // }


})