// pages/index/index.js
let utils = require('../../utils/utils');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    imgUrls: [],
    itemList: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {
    // 自动发放优惠券
    this.autoCoupons();
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    this.getBanner();
    this.getspotLists();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },

  // 跳转详情
  jumpDetail(e) {
    let id = e.currentTarget.dataset.id;
    let name = e.currentTarget.dataset.name;
    wx.navigateTo({
      url: "/pages/spotDetail/spotDetail?id=" + id + '&name=' + name
    })
  },

  // 获取景点轮播
  getBanner() {
    let that = this;
    utils.request({
      url: 'landmark/getScenicSlide',
      method: 'GET',
      success: function(res) {
        let r = res.data;
        if (r.success) {
          let data = r.data;
          that.setData({
            imgUrls: data
          })
        }
      }
    })
  },

  // 获取景点列表
  getspotLists() {
    let that = this;
    utils.request({
      url: 'landmark/getLandmarkList',
      method: 'GET',
      success: function(res) {
        let r = res.data;
        if (r.success) {
          let data = r.data;
          that.setData({
            itemList: data
          })
        }
      }
    })
  },

  // 自动发放优惠券
  autoCoupons() {
    utils.request({
      url: 'promotions/coupon/auto',
      success: function(res) {
        let r = res.data;
        if (r.success) {
          // console.log(r.msg);
        }
      }
    })
  },

})