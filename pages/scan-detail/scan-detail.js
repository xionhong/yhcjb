let utils = require('../../utils/utils');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    ticketCode: '',
    scanMsg: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(e) {
    let ticketCode = e.code;
    this.setData({
      ticketCode
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    this.getTicketInfo();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },

  getTicketInfo() {
    let that = this;
    let ticketCode = that.data.ticketCode;
    utils.request({
      url: 'order/getTicketInfo',
      data: {
        ticketCode
      },
      method: 'GET',
      success: function(res) {
        let r = res.data;
        if (r.success) {
          let data = r.data;
          that.setData({
            scanMsg: data
          })
        } else {
          wx.showModal({
            title: '提示',
            content: r.msg,
            showCancel: false,
            success: function() {
              wx.navigateBack({
                delta: 1,
              })
            }
          })
        }
      }
    })
  },

  // 拒绝
  refuseBtn() {
    wx.navigateBack({
      delta: 1,
    })
  },

  // 确认放行
  confirmBtn() {
    let that = this;
    let ticketCode = that.data.ticketCode;
    utils.request({
      url: 'order/confirmRelease',
      data: {
        ticketCode
      },
      success: function(res) {
        let r = res.data;
        if (r.success) {
          console.log(r);
          wx.showModal({
            title: '提示',
            content: r.msg,
            showCancel: false,
            success: function(re) {
              if (re.confirm) {
                wx.navigateBack({})
              }
            }
          })
        } else {
          wx.showModal({
            title: '提示',
            content: r.msg,
            showCancel: false,
            success: function(re) {
              if (re.confirm) {
                wx.navigateBack({})
              }
            }
          })
        }
      }
    })
  }
})