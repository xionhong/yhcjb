// map.js
let utils = require('../../utils/utils');
let QQMapWX = require('../../utils/qqmap-wx-jssdk.min.js');
let qqmapsdk;

Page({
  data: {
    mapLists: [],
    latitude: 0,
    longitude: 0,
    markers: [],
    locationId: ''
  },

  onLoad: function(e) {
    let locationId = e.id;
    if (locationId) {
      this.setData({
        locationId
      })
    }

    qqmapsdk = new QQMapWX({
      key: 'AZTBZ-YYJKX-BIE4S-ZUS7W-FVRB2-IUBGT'
    });
  },

  onShow: function() {
    if (this.data.locationId == '') {
      this.getLonLat();
    } else {
      this.getMap();
    }

    // 调用接口
    // qqmapsdk.search({
    //   keyword: '柚幻蔡家堡',
    //   success: function(res) {
    //     // console.log(res);
    //   }
    // })
  },

  // 地图列表
  getLonLat() {
    let that = this;
    utils.request({
      url: 'index/queryMapList',
      method: 'GET',
      success: function(res) {
        let r = res.data;
        if (r.success) {
          let data = r.data;
          let mapLists = data;
          that.setData({
            mapLists
          });
          let markers = that.data.markers;
          data.forEach(item => {
            let marker = that.createMarker(item);
            markers.push(marker);
          })
          that.setData({
            latitude: data[0].lat,
            longitude: data[0].lng,
            markers,
          })
        }
      }
    })
  },

  // 地图
  getMap() {
    let that = this;
    let mapId = that.data.locationId;
    utils.request({
      url: 'index/queryMapById',
      data: {
        mapId
      },
      success: function(res) {
        let r = res.data;
        if (r.success) {
          let data = r.data;
          let markers = that.data.markers;
          let marker = that.createMarker(data);
          markers.push(marker);
          that.setData({
            latitude: data.lat,
            longitude: data.lng,
            markers
          })
        }
      }
    })
  },

  // 获取地图列表
  createMarker(point) {
    let latitude = point.lat;
    let longitude = point.lng;
    let marker = {
      id: point.id,
      latitude: latitude,
      longitude: longitude,
      label: {
        content: point.name,
        color: '#e61c4c'
      },
      callout: {
        content: point.address,
        padding: '5rpx',
        borderRadius: '10rpx'
      },
      width: 30,
      height: 30
    };
    return marker;
  },

  markertap(e) {
    let mapId = e.markerId;
    utils.request({
      url: 'index/queryMapById',
      data: {
        mapId
      },
      success: function(res) {
        let r = res.data;
        if (r.success) {
          let data = r.data;
          wx.getLocation({ //gcj02
            type: 'wgs84', //返回可以用于wx.openLocation的经纬度
            success: (res) => {
              wx.openLocation({
                latitude: data.lat,
                longitude: data.lng,
                name: data.name,
                address: data.address
              })
            }
          });
        }
      }
    })
  }
})