let util = require('../../utils/util');
Page({
  data: {
    calendar: [],
    // 构建顶部日期时使用
    date: ['周日', '周一', '周二', '周三', '周四', '周五', '周六'],
    inTime: '',
    outTime: '',
    toDay: ''
  },

  // 日历初始化
  dataInit(setYear, setMonth) {
    // 当前时间/传入的时间
    var now = setYear ? new Date(setYear, setMonth) : new Date();
    var year = setYear || now.getFullYear();
    // 传入的月份已经加1
    var month = setMonth || now.getMonth() + 1;
    // 构建某日数据时使用
    var obj = {};
    // 需要遍历的日历数组数据
    var dateArr = [];
    // 需要的格子数，为当前星期数+当月天数
    var arrLen = 0;
    // 该月加1的数值，如果大于11，则为下年，月份重置为1 
    // 目标月1号对应的星期
    var startWeek = new Date(year + '-' + (month < 10 ? '0' + month : month) + '-01').getDay();
    //获取目标月有多少天
    var dayNums = new Date(year, month < 10 ? '0' + month : month, 0).getDate();
    var num = 0;
    // 计算当月需要的格子数 = 当前星期数+当月天数
    arrLen = startWeek * 1 + dayNums * 1;
    for (var i = 0; i < arrLen; i++) {
      if (i >= startWeek) {
        num = i - startWeek + 1;
        obj = {
          /*
           * 返回值说明
           * isToday ： 2018-12-27
           * dateNum :  27
           */
          isToday: year + '-' + (month < 10 ? '0' + month : month) + '-' + (num < 10 ? '0' + num : num),
          dateNum: num
        }
      } else {
        // 填补空缺
        // 例如2018-12月第一天是星期6，则需要补6个空格
        obj = {};
      }
      dateArr[i] = obj;
    };
    return dateArr;
  },
  // 点击了日期，选择入住时间或者离店时间
  dayClick(e) {
    var that = this;
    var eTime = e.currentTarget.dataset.day;
    var inTime = that.data.inTime;
    var outTime = that.data.outTime;

    let toDay = util.formatTime(new Date());
    if (eTime < toDay || eTime < "2019-02-05") {
      return false;
    };

    if (inTime == '' || (new Date(eTime) <= new Date(inTime)) || outTime != '') {
      // 如果入住时间为空或选择的时间小于等于入住时间，则选择的时间为入住时间
      inTime = eTime;
      outTime = '';
    } else {
      outTime = eTime;
    };
    that.setData({
      inTime,
      outTime
    })

    let pages = getCurrentPages();
    let prevPage = pages[pages.length - 2];
    // 入住时间
    let inDate = inTime.split('-');
    inDate = inDate[1] + '月' + inDate[2] + '日';
    // 退房时间
    let outDate = outTime.split('-');
    outDate = outDate[1] + '月' + outDate[2] + '日';
    // 共住几晚
    let totalNight = util.DateMinus(inTime, outTime);

    prevPage.setData({
      inDate,
      outDate,
      totalNight,
      inTime,
      outTime
    })
    if (outTime == '') {
      return false
    } else {
      wx.navigateBack({
        delta: 1,
      })
    }
  },
  onLoad(options) {
    var that = this;
    // 获取本月时间
    var nowTime = new Date();
    let toDay = util.formatTime(nowTime);
    var year = nowTime.getFullYear();
    var month = nowTime.getMonth();
    var time = [];
    var timeArray = {};
    // 循环6个月的数据
    for (var i = 0; i < 6; i++) {
      year = month + 1 > 12 ? year + 1 : year;
      month = month + 1 > 12 ? 1 : month + 1;
      // 每个月的数据
      time = that.dataInit(year, month);
      // 接收数据
      timeArray[year + '年' + (month < 10 ? '0' + month : month) + '月'] = time;
    };
    that.setData({
      calendar: timeArray,
      toDay
    });
  },
})