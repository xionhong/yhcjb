const app = getApp();
let url = app.globalData.url;
let header = app.globalData.header;
let util = require('../../utils/util');
let utils = require('../../utils/utils');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    num: 1,
    startDate: '',
    endDate: '',
    homestayId: '',
    price: '',
    inTime: '',
    outTime: '',
    allDay: '',
    payPrice: '',
    hotelName: '',
    prices: [],
    phone: '',
    // 实际支付的价格
    totalPrice: '',
    // 优惠券id
    couponId: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(e) {
    // console.log(e);
    this.setData({
      startDate: e.startDate,
      endDate: e.endDate,
      homestayId: e.id,
      price: e.price,
      deductPrice: e.deductPrice / 100,
      couponId: e.couponId ? e.couponId : ''
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    this.getHomestayMsg();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },

  addNum() {
    this.data.num += 1;
    // 将其赋值
    this.setData({
      num: this.data.num
    }, function() {
      let num = this.data.num;
      this.getHomestayMsg(num);
    });
  },

  // 相减
  redNum: function() {
    if (this.data.num <= 1) {
      return;
    }
    this.data.num -= 1;
    // 将其赋值
    this.setData({
      num: this.data.num
    }, function() {
      let num = this.data.num;
      this.getHomestayMsg(num);
    });
  },

  // 展示明细
  showOrderDetail() {
    let that = this;
    that.setData({
      showMask: true,
      showOrderDetail: true
    })
  },

  // 预定房间数据
  getHomestayMsg(num) {
    let that = this;
    num = that.data.num;
    let endDate = that.data.endDate;
    let startDate = that.data.startDate;
    let id = that.data.homestayId;
    let price = that.data.price;
    let couponId = that.data.couponId;
    utils.request({
      url: 'hotel/predestine',
      data: {
        amount: num,
        startDate,
        endDate,
        id,
        price,
        couponId
      },
      success: function(res) {
        let r = res.data;
        if (r.success) {
          let data = r.data;
          let inTime = util.turnDate(data.startDate);
          let outTime = util.turnDate(data.endDate);
          let allDay = data.day;
          let payPrice = Number(data.payPrice) / 100;
          let totalPrice = data.payPrice;
          let hotelName = data.roomRestVo.name;
          let prices = data.prices;
          let couponName = data.couponName == null || data.couponName == '' ? '使用优惠券' : data.couponName
          that.setData({
            inTime,
            outTime,
            allDay,
            payPrice,
            hotelName,
            prices,
            totalPrice,
            couponName
          })
        } else {
          wx.showModal({
            title: '提示',
            content: r.msg,
            showCancel: false,
            success: function() {
              that.data.num -= 1;
              // 将其赋值
              that.setData({
                num: that.data.num <= 0 ? 1 : that.data.num,
                couponId: ''
              }, function() {
                num = that.data.num;
                that.getHomestayMsg(num);
              });
            }
          })
        }
      }
    })
  },

  // 获取号码
  getPhone(e) {
    let that = this;
    that.setData({
      phone: e.detail.value
    })
  },

  // 提交订单
  formSubmit: util.repeatBtn(function(e) {
    let that = this;
    var arr = [];
    let obj = e.detail.value;
    for (let i in obj) {
      arr.push(obj[i]);
    }
    let checkUserName = arr;
    let userPhone = that.data.phone;
    let day = that.data.allDay;
    let amount = that.data.num;
    let startDate = that.data.startDate;
    let endDate = that.data.endDate;
    let payPrice = that.data.totalPrice;
    let roomName = that.data.hotelName;
    let id = that.data.homestayId;
    let couponId = that.data.couponId;
    for (let i = 0; i < checkUserName.length; i++) {
      if (checkUserName[i] == '') {
        wx.showModal({
          title: '提示',
          content: '入住人不能为空',
          showCancel: false
        })
        return
      }
    }
    if (userPhone == '') {
      wx.showModal({
        title: '提示',
        content: '手机号码不能为空',
        showCancel: false
      })
      return
    } else {
      utils.request({
        url: 'order/onlineHotelOrder',
        data: {
          checkUserName,
          userPhone,
          day,
          amount,
          startDate,
          endDate,
          payPrice,
          roomName,
          id,
          couponId
        },
        success: function(res) {
          let r = res.data;
          if (r.success) {
            let data = r.data;
            let homestayId = data.id;
            wx.requestPayment({
              timeStamp: data.wxPayMpOrderResult.timeStamp,
              nonceStr: data.wxPayMpOrderResult.nonceStr,
              package: data.wxPayMpOrderResult.packageValue,
              signType: data.wxPayMpOrderResult.signType,
              paySign: data.wxPayMpOrderResult.paySign,
              success: function(res) {
                wx.showModal({
                  title: '提示',
                  content: '支付成功',
                  showCancel: false,
                  success: function(r) {
                    if (r.confirm) {
                      wx.redirectTo({
                        url: '../homestay-order-detail/homestay-order-detail?homestayId=' + homestayId
                      })
                    }
                  }
                })
              },
              fail: function(res) {
                wx.showModal({
                  title: '提示',
                  content: '未支付成功',
                  showCancel: false,
                  success: function() {
                    wx.navigateBack({
                      delta: 1,
                    })
                  }
                })
              }
            })
          } else {
            wx.showModal({
              title: '提示',
              content: r.msg,
              showCancel: false
            })
          }
        }
      })
    }
  }, 1000),

  // 优惠券
  useCoupons() {
    let that = this;
    let payMoney = that.data.totalPrice;
    wx.navigateTo({
      //scope:1酒店 2景点
      url: '/pages/use-coupons/use-coupons?payMoney=' + payMoney + '&scope=' + 1,
    })
  },

})