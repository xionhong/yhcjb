// pages/my-coupons/my-coupons.js
let utils = require('../../utils/utils');
let page = 1;
let rows = 6;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    tabItem: ['全部', '未使用', '已使用', '已过期'],
    currentTabsIndex: 0,
    couponsLists: [],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(e) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {
    // 自动发放优惠券
    this.autoCoupons();
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    page = 1;
    this.data.couponsLists = [];
    this.getCouponsLists(page, '');
    this.setData({
      currentTabsIndex: 0
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    var that = this;
    let idx = that.data.currentTabsIndex;
    wx.showLoading({
      title: '玩命加载中'
    })
    page++;
    if (idx == 0) {
      that.getCouponsLists(page, '');
    } else if (idx == 1) {
      that.getCouponsLists(page, 0);
    } else if (idx == 2) {
      that.getCouponsLists(page, 1);
    } else {
      that.getCouponsLists(page, 3);
    }
    wx.hideLoading();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },

  // 切换栏
  onTabs(e) {
    let idx = e.currentTarget.dataset.index;
    let that = this;
    page = 1;
    that.setData({
      currentTabsIndex: idx
    })
    that.data.couponsLists = [];
    if (idx == 0) {
      that.getCouponsLists(page, '');
    } else if (idx == 1) {
      that.getCouponsLists(page, 0);
    } else if (idx == 2) {
      that.getCouponsLists(page, 1);
    } else {
      that.getCouponsLists(page, 3);
    }
  },

  // 获取我的优惠券列表
  getCouponsLists(page, status) {
    let that = this;
    utils.request({
      url: 'promotions/coupon/myCoupon',
      data: {
        page,
        rows,
        status
      },
      success: function(res) {
        let r = res.data;
        if (r.success) {
          let data = r.data;
          if (data.rows.length != 0) {
            data.rows.forEach(item => {
              that.data.couponsLists.push(item);
            })
          } else {
            wx.showModal({
              title: '提示',
              content: '暂无更多数据',
              showCancel: false
            })
          }
          that.setData({
            couponsLists: that.data.couponsLists
          })
        }
      }
    })
  },

  // 使用须知
  useNotice(e) {
    let url = e.currentTarget.dataset.url;
    if (url != '' && url != null) {
      wx.navigateTo({
        url: '/pages/payNotice/payNotice?notesurl=' + url,
      })
    } else {
      wx.showModal({
        title: '提示',
        content: '暂无使用须知！！！',
        showCancel: false
      })
    }
  },

  // 自动发放优惠券
  autoCoupons() {
    utils.request({
      url: 'promotions/coupon/auto',
      success: function(res) {
        let r = res.data;
        if (r.success) {
          // console.log(r.msg);
        }
      }
    })
  },

  // 使用优惠券
  useBtn(e) {
    let scope = e.currentTarget.dataset.scope;
    // console.log(scope);
    // 优惠券类型 1 酒店, 2 景点
    if (scope == 1) {
      wx.switchTab({
        url: '/pages/homestay/homestay'
      })
    } else if (scope == 2) {
      wx.switchTab({
        url: '/pages/spot/spot'
      })
    } else {
      wx.switchTab({
        url: '/pages/homestay/homestay'
      })
    }
  },

})