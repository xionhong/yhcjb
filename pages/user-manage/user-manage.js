const app = getApp();
let utils = require('../../utils/utils');
let page = 1;
let rows = 10;

Page({

  /**
   * 页面的初始数据
   */
  data: {
    userList: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    page = 1;
    this.data.userList = [];
    this.getUserList(page);
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    let that = this;
    // 显示加载图标
    wx.showLoading({
      title: '玩命加载中'
    })
    page += 1;
    that.getUserList(page);
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },

  // 新增游客
  adduserBtn() {
    wx.navigateTo({
      url: '../user-add/user-add'
    })
  },

  // 编辑
  editBtn(e) {
    let userId = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '../user-add/user-add?userId=' + userId
    })
  },

  // 删除
  delBtn(e) {
    let id = e.currentTarget.dataset.id;
    let that = this;
    wx.showModal({
      title: '提示',
      content: '是否删除？',
      success: function(re) {
        if (re.confirm) {
          utils.request({
            url: 'member/passengerDel',
            data: {
              id
            },
            success: function(res) {
              let r = res.data;
              if (r.success) {
                that.getUserList(page);
              }
            }
          })
        } else if (re.cancel) {}
      }
    })

  },

  // 游客列表
  getUserList(page) {
    let that = this;
    utils.request({
      url: '/member/passengerList',
      data: {
        page,
        rows
      },
      success: function(res) {
        let r = res.data;
        if (r.success) {
          let data = r.data;
          if (data.length != 0) {
            data.forEach(item => {
              that.data.userList.push(item);
            })
            that.setData({
              userList: that.data.userList
            })
          } else {
            that.setData({
              userList: data
            })
            wx.showModal({
              title: '提示',
              content: '暂无更多数据',
              showCancel: false
            })
          }
        }
      }
    })
  },

})