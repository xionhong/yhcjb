let utils = require('../../utils/utils');
let util = require('../../utils/util');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    siteName: '',
    num: 1,
    ticketTypeId: '',
    ticketPrice: '',
    totalPrice: '',
    inDate: '',
    eTime: '',
    userList: [],
    // idcardType: '',
    realName: '',
    phone: '',
    // idcard: '',
    // 实际支付的费用
    payPrice: '',
    // 优惠券id
    couponId: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(e) {
    this.setData({
      ticketTypeId: e.id,
      couponId: e.couponId ? e.couponId : ''
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    this.getTicketInfo();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },

  // 获取门票类型信息
  getTicketInfo(num) {
    let that = this;
    num = that.data.num;
    let ticketTypeId = that.data.ticketTypeId;
    let useDate = that.data.eTime;
    let couponId = that.data.couponId;
    utils.request({
      url: 'ticket/ticketInfo',
      data: {
        amount: num,
        ticketTypeId,
        couponId,
        useDate
      },
      success: function(res) {
        let r = res.data;
        if (r.success) {
          let data = r.data;
          num = that.data.num;
          that.setData({
            siteName: data.ticketName + data.ticketTypeName,
            ticketPrice: data.ticketPrice / 100,
            totalPrice: data.preferentialPrice / 100,
            payPrice: data.ticketPrice * num,
            couponName: data.couponName == null || data.couponName == '' ? '使用优惠券' : data.couponName
          })
        } else {
          wx.showModal({
            title: '提示',
            content: r.msg,
            showCancel: false,
            success: function() {
              that.data.num -= 1;
              // 将其赋值
              that.setData({
                num: that.data.num <= 0 ? 1 : that.data.num,
                couponId: ''
              }, function() {
                num = that.data.num;
                that.getTicketInfo(num);
              });
            }
          })
        }
      }
    })
  },

  // 选择日期
  bindCheckDate() {
    wx.navigateTo({
      url: '../calendar-spot/calendar-spot'
    })
  },

  addNum() {
    this.data.num += 1;
    // 将其赋值
    this.setData({
      num: this.data.num,
    });
    let num = this.data.num;
    this.getTicketInfo(num);
  },

  // 相减
  redNum: function() {
    if (this.data.num <= 1) {
      return;
    }
    this.data.num -= 1;
    // 将其赋值
    this.setData({
      num: this.data.num
    });
    let num = this.data.num;
    this.getTicketInfo(num);
  },

  // 选择游玩人
  bindPlayer() {
    let that = this;
    that.setData({
      showMask: true,
      showPlayer: true
    })
    utils.request({
      url: 'member/passengerList',
      data: {
        page: 1,
        rows: 20
      },
      success: function(res) {
        let r = res.data;
        if (r.success) {
          let data = r.data;
          that.setData({
            userList: data
          })
        }
      }
    })
  },

  //证件类型、
  // bindCardType() {
  //   let that = this;
  //   that.setData({
  //     showModel: true,
  //     showMask: true
  //   })
  //   utils.request({
  //     url: 'member/cardTypeList',
  //     success: function(res) {
  //       let r = res.data;
  //       if (r.success) {
  //         let data = r.data;
  //         that.setData({
  //           cardTypeList: data
  //         })
  //       }
  //     }
  //   })
  // },

  // 选择证件类型
  gettype: function(e) {
    // console.log(e)
    let that = this;
    // let idcardType = e.detail.idcardType;
    // let idcardName = e.detail.idcardName;
    let realName = e.detail.realName;
    let phone = e.detail.phone;
    // let idcard = e.detail.idcard;
    that.setData({
      // idcardType,
      realName,
      phone,
      // idcard
    })
  },

  // 提交订单
  submitOrder: util.repeatBtn(function(e) {
    // console.log(e);
    let that = this;
    let amount = that.data.num;
    // let idType = that.data.idcardType;
    // let idcard = e.detail.value.userId;
    let payPrice = that.data.payPrice;
    let phone = e.detail.value.userPhone;
    let realName = e.detail.value.userName;
    let ticketTypeId = that.data.ticketTypeId;
    let useDate = that.data.eTime;
    let couponId = that.data.couponId;
    if (useDate == '') {
      wx.showModal({
        title: '提示',
        content: '未选择使用日期',
        showCancel: false
      })
      return
    } else if (realName == '') {
      wx.showModal({
        title: '提示',
        content: '姓名不能为空',
        showCancel: false
      })
      return
    } else if (phone == '') {
      wx.showModal({
        title: '提示',
        content: '手机号码不能为空',
        showCancel: false
      })
      return
    }
    // else if (idType == '') {
    //   wx.showModal({
    //     title: '提示',
    //     content: '未选择证件类型',
    //     showCancel: false
    //   })
    //   return
    // } 
    // else if (idcard == '') {
    //   wx.showModal({
    //     title: '提示',
    //     content: '证件号不能为空',
    //     showCancel: false
    //   })
    //   return
    // } 
    else {
      utils.request({
        url: 'order/onlineTkOrder',
        data: {
          amount,
          // idType,
          // idcard,
          payPrice,
          phone,
          realName,
          ticketTypeId,
          useDate,
          couponId
        },
        success: function(res) {
          let r = res.data;
          if (r.success) {
            let data = r.data;
            let orderid = data.id;
            wx.requestPayment({
              timeStamp: data.wxPayMpOrderResult.timeStamp,
              nonceStr: data.wxPayMpOrderResult.nonceStr,
              package: data.wxPayMpOrderResult.packageValue,
              signType: data.wxPayMpOrderResult.signType,
              paySign: data.wxPayMpOrderResult.paySign,
              success: function(res) {
                wx.showModal({
                  title: '提示',
                  content: '支付成功',
                  showCancel: false,
                  success: function(r) {
                    if (r.confirm) {
                      wx.redirectTo({
                        url: '../spot-order-detail/spot-order-detail?orderid=' + orderid
                      })
                    }
                  }
                })
              },
              fail: function(res) {
                wx.showModal({
                  title: '提示',
                  content: '未支付成功',
                  showCancel: false,
                  success: function() {
                    wx.navigateBack({
                      delta: 1,
                    })
                  }
                })
              }
            })
          } else {
            wx.showModal({
              title: '提示',
              content: r.msg,
              showCancel: false
            })
          }
        }
      })
    }
  }, 1000),

  // 优惠券
  useCoupons() {
    let that = this;
    let payMoney = that.data.payPrice;
    wx.navigateTo({
      //scope:1酒店 2景点
      url: '/pages/use-coupons/use-coupons?payMoney=' + payMoney + '&scope=' + 2,
    })
  },

})