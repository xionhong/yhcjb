Page({

  /**
   * 页面的初始数据
   */
  data: {
    imgUrls: [
      '/images/ban1.png',
      '/images/ban2.png',
      '/images/ban3.png'
    ],
    hotelList: [{
        id: '1',
        img: '/images/list1.png',
        name: '景点',
        num: '2',
        money: '90',
        beforeMoney: '100'
      },
      {
        id: '2',
        img: '/images/list1.png',
        name: '景点',
        num: '1',
        money: '80',
        beforeMoney: '90'
      }
    ],
    siteName: '民宿名称民宿名称',
  },

  // 拼团规则
  ruleDetails() {
    wx.navigateTo({
      url: '/pages/rule-details/rule-details'
    })
  },

  // 查看详情
  payNotice() {
    wx.navigateTo({
      url: '/pages/pay-notice/pay-notice'
    })
  },

  // 去参团
  goGroupBuying(e) {
    let orderid = '';
    wx.navigateTo({
      url: '/pages/submit-order/submit-order?orderid=' + orderid
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})