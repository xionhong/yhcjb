// pages/my/my.js
const app = getApp();
let utils = require('../../utils/utils');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    admin: false,
    showPower: false,
    callUser: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    // console.log(app.globalData.userInfo);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    this.autoLogin()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  // 登录
  autoLogin: function() {
    let that=this;
    wx.login({
      success: r => {
        // console.log(r);
        // 发送 r.code 到后台换取 openId, sessionKey, unionId
        wx.request({
          url: getApp().globalData.url + 'member/autoLogin',
          data: {
            code: r.code
          },
          header: {
            "Content-Type": "application/x-www-form-urlencoded",
          },
          method: 'POST',
          success: result => {
            // console.log(result);
            let resultData = result.data;
            if (resultData.success) {
              let data = resultData.data;
              that.setData({
                admin: data.admin
              })
            }
          }
        });
      }
    })
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },

  // 通用码
  commonCode() {
    let that = this;
    let sessionId = app.globalData.sessionId;
    let timestamp = Date.parse(new Date());
    console.log(timestamp);
    let codeImg = app.globalData.url + 'member/adminTicketCode?JSESSIONID=' + sessionId + '&' + timestamp;
    that.setData({
      showMask: true,
      showQrcode: true,
      codeImg
    })
  },

  // 扫码检票
  scanCodes() {
    wx.scanCode({
      success(res) {
        let code = res.result;
        wx.navigateTo({
          url: '../scan-detail/scan-detail?code=' + code
        })
      }
    })
  },

  // 联系景区
  callShowPower() {
    let that = this;
    that.setData({
      showPower: true,
      showMask: true
    })
    utils.request({
      url: 'landmark/getScenic',
      method: 'GET',
      success: function(res) {
        let r = res.data;
        if (r.success) {
          let data = r.data;

          that.setData({
            callUser: data.phone
          })
        }
      }
    })
  },
})