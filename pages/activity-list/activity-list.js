Page({

  /**
   * 页面的初始数据
   */
  data: {
    itemList: [{
        id: '01',
        img: '/images/list1.png',
        name: '【门票】金惠园温泉酒店1',
        price: '￥500',
        type: '1'
      },
      {
        id: '02',
        img: '/images/list1.png',
        name: '【民宿】金惠园温泉酒店2',
        price: '￥500',
        type: '2'
      },
      {
        id: '03',
        img: '/images/list1.png',
        name: '【民宿】金惠园温泉酒店3',
        price: '￥500',
        type: '2'
      },
      {
        id: '04',
        img: '/images/list1.png',
        name: '【门票】金惠园温泉酒店4',
        price: '￥500',
        type: '1'
      }
    ]
  },

  // 跳转详情
  jumpDetail(e) {
    let id = e.currentTarget.dataset.id;
    let type = e.currentTarget.dataset.type;
    if (type == 1) {
      wx.navigateTo({
        url: "/pages/spot-group-buying/spot-group-buying?id=" + id
      })
    } else {
      wx.navigateTo({
        url: "/pages/homestay-group-buying/homestay-group-buying?id=" + id
      })
    }
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    wx.setNavigationBarTitle({
      title: options.title
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})