let utils = require('../../utils/utils');
let page = 1;
let rows = 10;

Page({

  /**
   * 页面的初始数据
   */
  data: {
    tabItem: ['全部', '待支付', '未使用'],
    currentTabsIndex: 0,
    orderType: '',
    orderStatus: '',
    orderList: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(e) {
    let orderType = e.type;
    this.setData({
      orderType
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    page = 1;
    this.data.orderList = [];
    this.orderList(page, '');
    this.setData({
      currentTabsIndex: 0
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    var that = this;
    let idx = that.data.currentTabsIndex;
    wx.showLoading({
      title: '玩命加载中'
    })
    page++;
    if (idx == 0) {
      that.orderList(page, '');
    } else if (idx == 1) {
      that.orderList(page, 0);
    } else {
      that.orderList(page, 1);
    }
    wx.hideLoading();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },

  // 切换栏
  onTabs(e) {
    let idx = e.currentTarget.dataset.index;
    let that = this;
    page = 1;
    that.setData({
      currentTabsIndex: idx
    })
    that.data.orderList = [];
    if (idx == 0) {
      that.orderList(page, '');
    } else if (idx == 1) {
      that.orderList(page, 0);
    } else {
      that.orderList(page, 1);
    }
  },

  // 订单列表
  orderList(page, orderStatus) {
    let that = this;
    let orderType = that.data.orderType;
    utils.request({
      url: 'order/list',
      data: {
        page,
        orderStatus,
        rows,
        orderType
      },
      success: function(res) {
        let r = res.data;
        if (r.success) {
          let data = r.data;
          if (data.rows.length != 0) {
            data.rows.forEach(item => {
              that.data.orderList.push(item);
            })
          } else {
            wx.showModal({
              title: '提示',
              content: '暂无更多数据',
              showCancel: false
            })
          }
          that.setData({
            orderList: that.data.orderList
          })
        }
      }
    })
  },

  // 取消订单
  delOrder(e) {
    let id = e.currentTarget.dataset.id;
    let that = this;
    let idx = that.data.currentTabsIndex;
    wx.showModal({
      title: '提示',
      content: '是否删除该订单？',
      success: function(re) {
        if (re.confirm) {
          utils.request({
            url: 'order/cancelOrder',
            data: {
              id
            },
            success: function(res) {
              let r = res.data;
              if (r.success) {
                that.onShow();
              }
            }
          })
        }
      }
    })

  },

  // 申请退款
  refundOrder(e) {
    let id = e.currentTarget.dataset.id;
    let that = this;
    let idx = that.data.currentTabsIndex;
    wx.showModal({
      title: '提示',
      content: '是否申请退款？',
      success: function(re) {
        if (re.confirm) {
          utils.request({
            url: 'order/cancelOrder',
            data: {
              id
            },
            success: function(res) {
              let r = res.data;
              if (r.success) {
                that.onShow();
              }
            }
          })
        }
      }
    })

  },

  // 去付款
  goPayment(e) {
    let that = this;
    let id = e.currentTarget.dataset.id;
    utils.request({
      url: 'order/paymentOrder',
      data: {
        id
      },
      success: function(res) {
        let r = res.data;
        if (r.success) {
          let data = r.data;
          wx.requestPayment({
            timeStamp: data.wxPayMpOrderResult.timeStamp,
            nonceStr: data.wxPayMpOrderResult.nonceStr,
            package: data.wxPayMpOrderResult.packageValue,
            signType: data.wxPayMpOrderResult.signType,
            paySign: data.wxPayMpOrderResult.paySign,
            success: function(res) {
              wx.showModal({
                title: '提示',
                content: '支付成功',
                showCancel: false
              })
            },
            fail: function(res) {
              wx.showModal({
                title: '提示',
                content: '未支付成功',
                showCancel: false
              })
            }
          })
        }
      }
    })
  },

  // 订单详情
  goOrderDetail(e) {
    let orderid = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '../spot-order-detail/spot-order-detail?orderid=' + orderid
    })
  },

})