


// pages/get-coupons/get-coupons.js
let utils = require('../../utils/utils');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    couponsLists: [],
    scope: '',
    payMoney: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(e) {
    let scope = e.scope;
    let payMoney = e.payMoney;
    this.setData({
      scope,
      payMoney
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    this.getCouponsLists();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },

  // 获取优惠券列表
  getCouponsLists() {
    let that = this;
    let scope = that.data.scope;
    let amount = that.data.payMoney;
    utils.request({
      url: 'promotions/coupon/select',
      data: {
        scope,
        amount
      },
      success: function(res) {
        let r = res.data;
        if (r.success) {
          let data = r.data;
          that.setData({
            couponsLists: data
          })
        }
      }
    })
  },

  // 使用优惠券
  useBtn(e) {
    let deductPrice = e.currentTarget.dataset.money;
    let couponId = e.currentTarget.dataset.id;
    let pages = getCurrentPages();
    let prevPage = pages[pages.length - 2];
    prevPage.setData({
      deductPrice: deductPrice,
      couponId: couponId
    })
    wx.navigateBack({
      delta: 1,
    })
  },

  // 使用须知
  useNotice(e) {
    let url = e.currentTarget.dataset.url;
    if (url != '' && url != null) {
      wx.navigateTo({
        url: '/pages/payNotice/payNotice?notesurl=' + url,
      })
    } else {
      wx.showModal({
        title: '提示',
        content: '暂无使用须知！！！',
        showCancel: false
      })
    }
  },
})