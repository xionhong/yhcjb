//app.js
let utils = require('utils/utils');
App({
  onLaunch: function() {
    utils.getSessionId();
    this.getUserInfo();
  },

  // 获取用户信息
  getUserInfo() {
    wx.getSetting({
      success: res => {
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
          wx.getUserInfo({
            success: res => {
              // 可以将 res 发送给后台解码出 unionId
              this.globalData.userInfo = res.userInfo

              // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
              // 所以此处加入 callback 以防止这种情况
              if (this.userInfoReadyCallback) {
                this.userInfoReadyCallback(res)
              }
            }
          })
        }
      }
    })
  },

  globalData: {
    // url: 'https://www.zgyouhuan.com/api/',
    // url: 'https://dev.zgyouhuan.com/api/',
    url: 'http://47.105.48.86:8081/scenic-hotel-dev/api/',
    sessionId: '',
    code: '',
    isAdmin: false,
    userInfo: {}
  }
})